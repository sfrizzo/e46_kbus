This version uses an I-Bus library to make things cleaner and simpler. All of the I/K-Bus communications is done using the library. 
Place the �Ibus� folder into your Arduino IDE libraries location. This is typically:
C:\Users\<username>\Documents\Arduino\libraries\


More information is available at:
http://curious.ninja/blog/arduino-bmw-ik-bus-interface-programming-v010beta/


